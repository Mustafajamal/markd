<?php
/**
 * Template Name: Home Page Template
 *
 * The template for Home Page.
 *
 * @package Hestia
 * @since Hestia 1.0
 */

get_header();

/**
 * Don't display page header if header layout is set as classic blog.
 */
do_action( 'hestia_before_single_page_wrapper' ); ?>


<style type="text/css">
	
	h1.hestia-title.title-in-content {
    display: none;
}
</style>
<script type="text/javascript">
	jQuery(document).ready(function() {
	  jQuery( ".gallery_more" ).click(function() {
	  jQuery( ".load_more_row" ).toggle( "slow", function() {
	    // Animation complete.
	  });
	  jQuery(this).text(function(i, v){
               return v === 'Load More' ? 'Load Less' : 'Load More'
        })

	  });
	});
</script>
    <div class="blog-post ">
			<?php
			$args = array( 'post_type' => 'gallery', 'posts_per_page' => 18, 'order'    => 'ASC' );
			
			
			$loop = new WP_Query( $args );
			echo '<div class="content homepage animated fadeInDown" id="homepage_gallery" style="display: block;"><div class="container"><div class="row templatemorow">';
			$n = 1;
			while ( $loop->have_posts() ) : $loop->the_post();
			 $link_location = get_post_meta( get_the_ID(), 'image_link', true );
			$link_label = get_post_meta( get_the_ID(), 'link_label', true );

			  ?><div class="hex col-sm-6  <?php if($n == 4 || $n == 5 ) echo 'templatemo-hex-top3'; ?><?php if($n == 3) echo 'templatemo-hex-top2'; ?> <?php if($n == 6 || $n == 15) echo 'hex-offset templatemo-hex-top1 templatemo-hex-top2'; ?> <?php if($n == 7 || $n == 8) echo ' templatemo-hex-top1 templatemo-hex-top3'; ?> <?php if($n == 9) echo 'templatemo-hex-top1 templatemo-hex-top2'; ?> <?php if($n == 16 || $n == 17) echo ' templatemo-hex-top1 templatemo-hex-top3'; ?> <?php if($n == 18) echo 'templatemo-hex-top1 templatemo-hex-top2'; ?> ">
		    	<div>
		          <div class="hexagon hexagon2 gallery-item">
		            <div class="hexagon-in1">
			              <div class="hexagon-in2" style="background-image: url( <?php the_post_thumbnail_url(); ?> );">
			             	<div class="overlay" style="display: none;">
			             			<?php if($link_location ){?>
			             				<a class="custom_link" href="<?php echo $link_location; ?>" ><?php echo $link_label;?></a>
			             			<?php } else{ ?>
									<a href="<?php the_post_thumbnail_url(); ?>" data-rel="lightbox" class="fa fa-expand"></a>
								<?php }  ?>
							  </div>
			                </div>
			              </div>
			        </div>
		             </div>
		  	    </div>

		  	    <?php
		  	    
		  	    if($n == 9) echo '</div><div id="newpost" style="display: none;" class="container load_more_row templatemo_gallerytop"><div class="row templatemorow">';

				?><?php
			  $n ++;
			endwhile; ?>
			 <?php echo '</div></div><div class="container">
		    	<div class="row">
		        	<div class="templatemo_loadmore">
					 <button class="gallery_more" id="button">Load More</button>
		            </div>
		        </div>
		    </div></div>'; ?>
	</div>

	<!-- Lightbox Start -->
	<div id="lightbox" style="display: none;">
		<a href="#" class="lightbox-close lightbox-button"></a>
		<div class="lightbox-nav">
			<a href="#" class="lightbox-previous lightbox-button"></a><a href="#" class="lightbox-next lightbox-button"></a>
		</div>
			<div href="#" class="lightbox-caption" style="display: none;"><p></p></div>
			<img src="images/gallery/16.jpg" draggable="false" style="display: block; width: 389px; height: 389px; top: 25px; left: 438px;">
	</div>
	<!-- Lightbox End -->


	<?php get_footer(); ?>
